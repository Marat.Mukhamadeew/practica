package edu.practica.basics;

/**
 * Реализуйте алгоритм сортировки пузырьком для сортировки массива
 * */

public class Task02 {

    public static void getSortedArray(int[] array) {

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }


    public static void main(String[] args) {

        int[] array = Task01.getArray(50);

        for (int i : array) {
            System.out.print(i + " ");
        }
        System.out.println();

        getSortedArray(array);

        for (int i : array) {
            System.out.print(i + " ");
        }
    }
}
