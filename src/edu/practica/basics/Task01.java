package edu.practica.basics;

import java.util.Arrays;

/**
 * Заполните массив случайным числами и выведите максимальное, минимальное и среднее значение.
 * <p>
 * Для генерации случайного числа используйте метод Math.random(), который возвращает значение в промежутке [0, 1].
 */


// Решение
public class Task01 {

    public static int[] getArray(int n) {
        int[] array = new int[n];

        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 100);
        }

        return array;
    }


    public static void main(String[] args) {
        int n = 100;
        int[] array = new int[n];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 100);
        }

        int max = array[0];
        int min = array[0];
        double avg = 0;
        int sum = 0;

        for (int d : array) {
            if (d > max) max = d;
            if (d < min) min = d;
            sum += d;
            avg = (double) sum / array.length;
        }

        System.out.println(Arrays.toString(array));
        System.out.println("max = " + max);
        System.out.println("min = " + min);
        System.out.println("avg = " + avg);

    }
}
