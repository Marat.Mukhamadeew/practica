package edu.practica.basics;

import java.util.Arrays;

/**
 * Напишите программу, которая выводит на консоль простые числа в промежутке от [2, 100].
 * Используйте для решения этой задачи оператор "%" (остаток от деления) и циклы.
 */

public class Task03 {

    public static int[] getPrimeNumbers(int[] array) {
        int counter = 0;
        int t = 0;

        for (int i = 2; i <= array.length; i++) {
            boolean isPrime = true;

            for (int j = 2; j < i; j++) {
                if (i % j == 0) {
                    isPrime = false;
                    break;
                }
            }
            if (isPrime) counter++;
        }
        int[] arr = new int[counter];

        for (int j = 2; j < array.length; j++) {
            boolean isPrime = true;

            for (int k = 2; k < j; k++) {
                if (j % k == 0) {
                    isPrime = false;
                    break;
                }
            }
            if (isPrime) {
                arr[t] = j;
                t++;
            }
        }
        return arr;
    }

    public static void main(String[] args) {
        for (int i = 2; i <= 100; i++) {
            boolean isPrime = true;

            for (int j = 2; j < i; j++) {
                if (i % j == 0) {
                    isPrime = false;
                    break;
                }
            }

            if (isPrime) {
                System.out.print(i + " ");
            }
        }
        System.out.println();

        out_for:
        for (int i = 2; i <= 100; i++) {
            for (int j = 2; j < i; j++) {
                if (i % j == 0) {
                    continue out_for;
                }
            }
            System.out.print(i + " ");
        }

        System.out.println();
        int[] array = getPrimeNumbers(Task01.getArray(100));
        System.out.println(Arrays.toString(array));
    }
}
